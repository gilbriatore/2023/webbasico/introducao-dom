const ultimoFilho = document.getElementById("pai").lastElementChild;
console.log(ultimoFilho);
//<p>Eu sou o último filho...</p>

const quartoFilho = document.getElementById("pai").children[3];
console.log(quartoFilho);
//<h1>Olá mundo!</h1>

const segundoFilho = document.getElementById("segundoFilho");
console.log(segundoFilho);
//<p id="segundoFilho">Eu sou o segundo filho...</p>

console.log(segundoFilho.parentNode);
//<div id="pai">...</div>

console.log(segundoFilho.nextElementSibling);
//<h4>Eu estou vivo...</h4>

console.log(segundoFilho.previousElementSibling);
//<div id="primeiroFilho">Eu sou o primeiro filho...</div>

//Criação de um elemento...
const divCriada = document.createElement("div");
console.log(divCriada);
//<div></div>

//Alteração do conteúdo HTML
divCriada.innerHTML = "Eu dou um desenvolvedor frontend...";
console.log(divCriada);
//<div>Eu dou um desenvolvedor frontend...​</div>​

//Seleção do elemento pai...
const pai = document.getElementById("pai");

//Inserção de elemento...
pai.appendChild(divCriada);
pai.appendChild(divCriada);
pai.appendChild(divCriada);
console.log(pai);

//Seleção do primeiro elemento...
const primeiroFilho = document.getElementById("primeiroFilho");

//Inserção de elemento antes...
divCriada.innerHTML = "Elemento inserido antes...";
pai.insertBefore(divCriada, primeiroFilho);

//Inserção de elemento depois...
divCriada.innerHTML = "Elemento inserido depois...";
pai.insertBefore(divCriada, primeiroFilho.nextElementSibling);
console.log(pai);

//Substituição de elementos...
divCriada.innerHTML = "Está substituindo o primeiro filho...";
pai.replaceChild(divCriada, primeiroFilho);
console.log(pai);

//Remoção do segundo elemento...
pai.removeChild(segundoFilho);
console.log(pai);

//Adicionar uma classe e um evento...
const btn = document.getElementById("btn");
btn.addEventListener("click", funcAddClasses);
function funcAddClasses() {
  document.body.classList.add("back");
  btn.classList.add("button");
}
